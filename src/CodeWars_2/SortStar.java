package CodeWars_2;

import java.util.ArrayList;
import java.util.Arrays;

public class SortStar {

    public static void main(String[] args) {
        System.out.println(twoSort(new String[]{"test", "cases", "are"}));
    }

    public static String twoSort(String[] s) {

        Arrays.sort(s);
        char[] a = s[0].toCharArray();

        String b = "";
        for (int i = 0; i <= a.length-1; i++){
           b += Character.toString(a[i]) + "***";
        }
        return b.substring(0, b.length() - 3);
    }

}
