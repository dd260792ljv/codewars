package CodeWars_2;

public class Repeatit {

    public static void main(String[] args) {
        System.out.println(repeatString("1", 11));
    }

    public static String repeatString(final Object toRepeat, final int n) {

        String a = "";

        if (toRepeat instanceof String) {
            for (int i = 0; i < n; i++) {
                a += toRepeat;
            }
        } else a = "Not a string";

        return a;

    }
}
