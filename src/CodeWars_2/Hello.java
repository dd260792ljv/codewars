package CodeWars_2;

public class Hello {

    public static void main(String[] args) {
        System.out.println(sayHello(new String[]{"John", "Smith"}, "Phoenix", "Arizona"));
    }


        public static String sayHello(String [] name, String city, String state){

            return String.format("Hello, %s! Welcome to %s, %s!",String.join(" ", name),city,state);
        }

}
