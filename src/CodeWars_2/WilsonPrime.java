package CodeWars_2;

import java.math.BigDecimal;
import java.math.BigInteger;

public class WilsonPrime {

    public static void main(String[] args) {
        System.out.println(am_i_wilson(563));
    }

    public static boolean am_i_wilson(double n) {
        if (n<2) return false;

        long l = (new Double(n)).longValue();
        BigInteger left = fact(n - 1).add(BigInteger.valueOf(1));
        BigInteger right = BigInteger.valueOf(l).multiply(BigInteger.valueOf(l));



        return left.mod(right).compareTo(BigInteger.valueOf(0)) == 0;

    }

    private static BigInteger fact(double num) {
        BigInteger f = BigInteger.valueOf(1);
        for (int i = 1; i <= num; i++) {
            f = f.multiply(BigInteger.valueOf(i));
        }
        return f;
    }

}
