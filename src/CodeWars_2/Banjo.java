package CodeWars_2;

public class Banjo {
    public static void main(String[] args) {
        System.out.println(areYouPlayingBanjo("Rartin"));
    }

    public static String areYouPlayingBanjo(String name) {
        return name + (name.toLowerCase().charAt(0) == 'r' ? " plays banjo" : " does not play banjo");
    }
}
