package CodeWars_2;

public class SetAlarm {
    public static void main(String[] args) {
        System.out.println(setAlarm(true, true));
    }

    public static boolean setAlarm(boolean employed, boolean vacation) {
        return employed && !vacation;
    }
}
