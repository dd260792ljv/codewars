package CodeWars_2;

import com.sun.deploy.util.ArrayUtil;
import com.sun.xml.internal.bind.v2.model.util.ArrayInfoUtil;

import java.util.*;

public class TwiceAsOld {
    public static void main(String[] args) {
        System.out.println(rentalCarCost(3));
    }

    public static int TwiceOld(int dadYears, int sonYears) {
        return Math.abs(dadYears - sonYears * 2);
    }

    public static int rentalCarCost(int d) {
        return d * 40 - (d >= 7 ? 50 : (d >= 3 ? 20 : 0));
    }

    public static String convert(boolean b) {
        return true ? "true" : "false";
    }

    public static int grow(int[] x) {

        int a = 1;
        for (int i = 0; i < x.length; i++) {
            a = a * x[i];
        }
        return a;
    }

}

