package CodeWars_2;

public class BasicMathematicalOperations {
    public static void main(String[] args) {
        System.out.println(isLove(1,4));
    }

    public static Integer basicMath(String op, int v1, int v2) {

        switch (op) {
            case "+": return v1 + v2;
            case "-": return v1 - v2;
            case "*": return v1 * v2;
            case "/": return v1 / v2;
        }
        return 0;
    }

    public static boolean isLove(final int flower1, final int flower2) {
        return flower1%2  == flower2%2 ? false : true;
    }
}
