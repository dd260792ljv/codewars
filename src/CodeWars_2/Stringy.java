package CodeWars_2;

import java.awt.*;
import java.util.Arrays;
import java.util.Objects;


public class Stringy {

    public static void main(String[] args) {
        System.out.println(findNeedle(new Object[]{"3", "123124234",  null, "needle", "world", "hay", 2, "3", true, false}));
    }

    public static String stringy(int size) {
        String s = "";
        for (int i = 1; i <= size; i++) {
            s += i % 2 != 0 ? "1" : "0";
        }
        return s;
    }

    public static boolean isDigit(String s) {
        return s.matches("[0-9]");
    }


    public static String buildString(String[] args) {
        return String.format("I like %s!", String.join(", ", args));
    }


    public static int[] map(int[] arr) {
        return Arrays.stream(arr).map(x -> x * 2).toArray();
    }


    public static String[] fixTheMeerkat(String[] arr) {

        String[] a = new String[arr.length];
        for (int i = arr.length - 1, j = 0; i >= 0; i--, j++) {
            a[j] = arr[i];
        }
        return a;
    }

    public static String findNeedle(Object[] haystack) {

        return "found the needle at position " + Arrays.asList(haystack).indexOf("needle");
    }
}

