package CodeWars_2;

import java.util.Arrays;
import java.util.stream.Stream;


public class SquareSum {

    public static void main(String[] args) {
        System.out.println();
    }

    public static int squareSum(int[] n) {
        return Arrays.stream(n).map(x -> x * x).sum();
    }

    public static int[] digitize(long n) {

        char[] a = Long.toString(n).toCharArray();
        int[] b = new int[a.length];

        for (int i = a.length - 1, j = 0; i >= 0; i--, j++) {
            b[j] = Integer.parseInt(String.valueOf(a[i]));
            ;
        }
        return b;
    }

    public static int arrayPlusArray(int[] arr1, int[] arr2) {

        return Stream.of(arr1, arr2).flatMapToInt(Arrays::stream).sum();
    }





}
