package CodeWars_2;

public class AbbreviateName {
    public static void main(String[] args) {
        System.out.println(abbrevName("am arris"));
    }

    public static String abbrevName(String name) {
        String [] names = name.split(" ");
        return (names[0].charAt(0) + "." + names[1].charAt(0)).toUpperCase();
    }
}

