package CodeWars_2;

import static java.util.stream.Collectors.joining;

public class FakeBinary {
    public static void main(String[] args) {
        System.out.println(fakeBin("1357"));
    }

    public static String fakeBin(String numberString) {

        return numberString.chars().mapToObj(x -> x < '5' ? "0" : "1").collect(joining());

    }
}
