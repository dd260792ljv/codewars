package CodeWars_2;

import com.sun.deploy.util.ArrayUtil;

import java.util.ArrayList;
import java.util.Arrays;

public class DivisibleByGivenNumber {
    public static void main(String[] args) {

        int[] a = new int[] {1, 2, 3, 4, 5, 6};
        System.out.println(Arrays.toString(divisibleBy(a, 2)));

    }

    public static int[] divisibleBy(int[] numbers, int divider) {

        ArrayList<Integer> result = new ArrayList<Integer>();

        for (int i = 0; i < numbers.length; i++) {
            if (numbers[i] % divider == 0)
                result.add(numbers[i]);
        }

        return result.stream().mapToInt(i -> i).toArray();

    }
}
