package CodeWars_2;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Arrays;

import static oracle.jrockit.jfr.events.Bits.intValue;
import static oracle.jrockit.jfr.events.Bits.longValue;

public class NeedOne {

    public static void main(String[] args) {
       System.out.println(check(new Object[] {66, 101},66));
    }


    public static boolean check(Object[] a, Object x) {
        return Arrays.stream(a).filter(i -> i.equals(x)).findAny().isPresent();
    }


}







