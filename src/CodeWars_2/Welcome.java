package CodeWars_2;

public class Welcome {
    public static void main(String[] args) {
        System.out.println(hello("czech"));
    }

    public static String hello(String lang) {

        String answer;
        switch (lang) {
            case "english":
                answer = "Welcome";
                break;
            case "czech":
                answer = "Vitejte";
                break;
            case "danish":
                answer = "Velkomst";
                break;
            case "dutch":
                answer = "Welkom";
                break;
            case "estonian":
                answer = "Tere tulemast";
                break;
            case " finnish":
                answer = "Tervetuloa";
                break;
            case "flemish":
                answer = "Welgekomen";
                break;
            case "french":
                answer = "Bienvenue";
                break;
            case "german":
                answer = "Willkommen";
                break;
            case "irish":
                answer = "Failte";
                break;
            case "italian":
                answer = "Benvenuto";
                break;
            case "latvian":
                answer = "Gaidits";
                break;
            case "lithuanian":
                answer = "Laukiamas";
                break;
            case "polish":
                answer = "Witamy";
                break;
            case "spanish":
                answer = "Bienvenido";
                break;
            case "swedish":
                answer = "Valkommen";
                break;
            case "welsh":
                answer = "Croeso";
                break;
            case "IP_ADDRESS_INVALID":
                answer = "not a valid ipv4 or ipv6 ip address";
                break;
            case "IP_ADDRESS_NOT_FOUND":
                answer = "ip address not in the database";
                break;
            case "IP_ADDRESS_REQUIRED":
                answer = "no ip address was supplied";
                break;
            default:
                answer = "Welcome";
                break;
        }

        return answer;


    }

}
