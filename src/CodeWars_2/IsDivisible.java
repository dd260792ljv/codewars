package CodeWars_2;

public class IsDivisible {

    public static void main(String[] args) {
        System.out.println(isDivisible(3,1,3));
    }

    public static boolean isDivisible(long n, long x, long y) {
        return n%x + n%y == 0;
    }

}
