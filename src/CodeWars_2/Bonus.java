package CodeWars_2;

import java.util.ArrayList;
import java.util.Arrays;

public class Bonus {

    public static void main(String[] args) {
        System.out.println(reverseWords("I like eating"));
    }

    public static String bonusTime(final int salary, final boolean bonus) {
        return  "\u00A3" + (bonus ? 10 : 1) * salary;
    }

    public static int stringToNumber(String str) {
        return Integer.parseInt(str);
    }

    public static String reverseWords(String str){

        String [] arr = str.split(" ");
        String [] b = new String[arr.length];

        for (int i = arr.length-1, j = 0; i>=0; i--, j++){
            b[j] = arr[i];
        }

        return  String.join(" ",b);
    }


}
