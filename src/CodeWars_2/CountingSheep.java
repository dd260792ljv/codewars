package CodeWars_2;

import java.lang.reflect.Array;
import java.util.ArrayList;

public class CountingSheep {
    public static void main(String[] args) {

        Boolean [] arr = new Boolean[]{true,  null, true,  false,
                true,  true,  true,  true ,
                true,  false, true,  false,
                true,  false, false, true ,
                true,  true,  true,  true ,
                false, false, true,  true};

        System.out.println(countSheeps(arr));
    }

    public static int countSheeps(Boolean[] arrayOfSheeps) {

        int result = 0;
        for (int i = 0; i <= arrayOfSheeps.length-1; i++){
            if(arrayOfSheeps[i] != null && arrayOfSheeps[i]){
                result ++;
            }
        }
        return result;
    }
}
