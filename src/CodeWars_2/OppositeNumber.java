package CodeWars_2;

public class OppositeNumber {

    public static void main(String[] args) {
        System.out.println(opposite(1));
    }

    public static int opposite(int number) {
        return Math.negateExact(number);
    }
}
