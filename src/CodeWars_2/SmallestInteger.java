package CodeWars_2;

import java.util.Arrays;

public class SmallestInteger {
    public static void main(String[] args) {
        System.out.println(howOld("5 years old"));
    }


    public static int findSmallestInt(int[] args) {
        Arrays.sort(args);
        return args[0];
    }

    public static int howOld(final String herOld) {
        return Integer.parseInt(herOld.split(" ")[0]);
    }
}
