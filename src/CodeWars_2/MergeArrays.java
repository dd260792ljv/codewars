package CodeWars_2;

import java.util.Arrays;
import java.util.TreeSet;

public class MergeArrays {

    public static void main(String[] args) {
        int[] a = {1,4,6};
        int[] b = {2,5};
        System.out.println(Arrays.toString(mergeArray(a, b)));
    }

    public static int[] mergeArray(int[] first, int[] second) {

        TreeSet<Integer> myTreeSet = new TreeSet<>();

        for(int i: first){
            myTreeSet.add(i);
        }

        for(int j: second){
            myTreeSet.add(j);
        }

        return  myTreeSet.stream().mapToInt(i -> i).toArray();
    }

}
