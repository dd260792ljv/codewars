package CodeWars;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;


public class ToLeetSpeak {
    public static void main(String[] args) {
        System.out.println(toLeetSpeak("LOREM IPSUM DOLOR SIT AMET"));
    }

    static String toLeetSpeak(final String speak) {


        Map<String, String> hashMap = new HashMap<String, String>();
        hashMap.put("A", "@");
        hashMap.put("B", "8");
        hashMap.put("C", "(");
        hashMap.put("D", "D");
        hashMap.put("E", "3");
        hashMap.put("F", "F");
        hashMap.put("G", "6");
        hashMap.put("H", "#");
        hashMap.put("I", "!");
        hashMap.put("J", "J");
        hashMap.put("K", "K");
        hashMap.put("L", "1");
        hashMap.put("M", "M");
        hashMap.put("N", "N");
        hashMap.put("O", "0");
        hashMap.put("P", "P");
        hashMap.put("Q", "Q");
        hashMap.put("R", "R");
        hashMap.put("S", "$");
        hashMap.put("T", "7");
        hashMap.put("U", "U");
        hashMap.put("V", "V");
        hashMap.put("W", "W");
        hashMap.put("X", "X");
        hashMap.put("Y", "Y");
        hashMap.put("Z", "2");

        String sp = speak.toUpperCase();
        String answer = "";


        for (char c : sp.toCharArray()) {
            String key = String.valueOf(c);
            answer += (hashMap.containsKey(key)) ?  hashMap.get(key) : key;
            }

        return answer;
    }
}



