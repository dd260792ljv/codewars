package CodeWars;

public class CaffeineScript {
    public static void main(String[] args) {
        System.out.println(caffeineBuzzTwo(12));
    }

    public static String caffeineBuzz(int n){


        String s = "Script";
        String answer = "";

        if(n%3 == 0 && n%4 == 0 && n%2 == 0){
            answer = "CoffeeScript";
        }else if(n%3 == 0 && n%2 == 0) {
            answer = "JavaScript";
        }else if(n%3 == 0) {
            answer = "Java";
        }else if(n%3 == 0 && n%4 == 0){
            answer = "Coffee";
        }else answer = "mocha_missing!";


        return answer;


        }

    public static String caffeineBuzzTwo (int n){


        String answer = "";

        if(n%3 == 0 && n%4 == 0) {
            answer = "Coffee";
        }else if(n%3 == 0) {
            answer = "Java";
        }else return  "mocha_missing!";

        return n%2 == 0 ? answer +"Script" : answer;

    }

    public static String caffeineBuzzThree (int n){
        if (n % 3 != 0)
            return "mocha_missing!";
        String prefix = n % 4 == 0 ? "Coffee" : "Java";
        return n%2 == 0 ? prefix + "Script" : prefix;
    }
}
