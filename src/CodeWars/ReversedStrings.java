package CodeWars;

public class ReversedStrings {
    public static void main(String[] args) {
        System.out.println(solutionOne("12345678910111213141516"));
    }

    public static String solutionOne(String str) {
        return new StringBuffer(str).reverse().toString();
    }

    //---------------------------------------------------------------

    public static String solutionTwo(String str) {

        String myStr = "";
        for (int i = str.length()-1; i>=0; i--){
            myStr += str.charAt(i);
        }
        return myStr;
    }
}
