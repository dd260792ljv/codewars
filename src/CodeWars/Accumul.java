package CodeWars;


public class Accumul {
    public static void main(String[] args) {
        String s = "abcd";
        System.out.println(accum(s));
    }

    public static String accum(String s) {
        String exit = "";
        char[] strToArray = s.toCharArray();

        for (int i = 0; i < strToArray.length; i++) {
            String line = "";
            for (int j = 0; j < i + 1; j++) {
                line = line + strToArray[i];
            }
            line = line.substring(0, 1).toUpperCase() + line.substring(1).toLowerCase();
            if (i == 0) {
                exit = exit + line;
            } else {
                exit = exit + "-" + line;
            }
        }
        return exit;
    }
}