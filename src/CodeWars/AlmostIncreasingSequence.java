package CodeWars;


public class AlmostIncreasingSequence {
    public static void main(String[] args) {
        int [] array = {3, 5, 67, 98, 3};

        System.out.println(almostIncreasingSequence(array));

    }

    public static boolean almostIncreasingSequence(int[] sequence) {

        if (sequence.length < 2) {
            throw new IllegalArgumentException("Must be two or more elements");
        }

        if (increasingSequence(sequence)) {
            return true;
        }

        int[] subSequence = new int[sequence.length - 1];

        for (int i = 0; i < sequence.length; i++) {
            for (int j = 0; j < subSequence.length; j++) {
                subSequence[j] = sequence[j >= i ? j + 1 : j];
                if (increasingSequence(subSequence)) {
                    return true;
                }
            }
        }
        return false;
    }

    private static boolean increasingSequence(int[] sequence) {

        for (int i = 0; i < sequence.length - 1; i++) {
            if (sequence[i] >= sequence[i + 1]) {
                return false;
            }
        }
        return true;
    }
}

/*for(int i = 0; i<sequence.length-2;) {
        for(int j = 0; j<sequence.length-2;){
            if (sequence[j] > sequence[j + 1] || sequence[j] > sequence[j + 2])
                j++;
            else return false;
        }
        if (sequence[i] < sequence[i + 1] || sequence[i] < sequence[i + 2])
            i++;
        else return false;
    }
        return true;
}*/
