package CodeWars;

public class ExesOhs {
    public static void main(String[] args) {
        System.out.println(getXO("zzoo"));
    }

    public static boolean getXO (String str) {

        char[] letters = str.toLowerCase().toCharArray();
        int o = 0;
        int x = 0;

        for(int i = 0; i < letters.length; i++){
            if(letters[i] == 'o'){
                o++;
            } else if (letters[i] == 'x'){
                x++;
            }
        }

        return o == x;
    }
}
