package CodeWars;

public class Kata2 {

    static int n;
    public Kata2(int n){
       Kata2.n = n;
    }

    public static Kata2 add(int n) {
        return new Kata2(n);
    }

    public static int method(int n){
        return Kata2.n + n;
    }
}
