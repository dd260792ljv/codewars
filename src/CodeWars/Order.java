package CodeWars;

import java.util.Arrays;

import static java.util.Arrays.sort;

public class Order {

    public static void main(String[] args) {
        System.out.println(order("is2 Thi1s T4est 3a"));
    }


    public static String order(String words) {
        String answer = "";

        String[] subStr;
        String[] subStr2;
        String delimeter = "-";


        subStr = words.split(delimeter);
        sort(subStr);
        return Arrays.toString(subStr);
    }
}


//https://www.codewars.com/kata/55c45be3b2079eccff00010f/train/java