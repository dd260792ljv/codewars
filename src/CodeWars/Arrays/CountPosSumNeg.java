package CodeWars.Arrays;

import java.util.Arrays;

public class CountPosSumNeg {
    public static void main(String[] args) {
        int[] array = null;
        System.out.println(Arrays.toString(countPositivesSumNegatives(array)));
    }

    public static int[] countPositivesSumNegatives(int[] arr) {

        if(arr == null ||  arr.length == 0) return new int[]{};

        int[] answer = new int[2];

        for (int i : arr) {
            if (i > 0) answer[0]++;
            else answer[1] += i;
        }

        return answer;
    }
}
