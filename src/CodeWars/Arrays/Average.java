package CodeWars.Arrays;

public class Average {
    public static void main(String[] args) {
        int [] array = {1,2,3,4,5};
        System.out.println(getAverage(array));
    }

    public static int getAverage(int[] marks){
        int answer = 0;
        for(int i : marks){
            answer += i;
        }
        return answer / marks.length;
    }
}
