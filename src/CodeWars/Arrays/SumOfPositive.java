package CodeWars.Arrays;

public class SumOfPositive {
    public static void main(String[] args) {
        int[] array = {1, -4, 7, 12};
        System.out.println(sum(array));
    }

    public static int sum(int[] arr) {
        int answer = 0;
        for (int i : arr) {
            if (i > 0) answer += i;
        }
        return answer;
    }
}
