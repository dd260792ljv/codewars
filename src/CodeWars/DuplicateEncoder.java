package CodeWars;

import java.util.ArrayList;
import java.util.List;

public class DuplicateEncoder {
    public static void main(String[] args) {
        System.out.println(encode("Prespecialized"));
    }

    static String encode(String word){

        char[] letters = word.toLowerCase().toCharArray();
        List<Character> duplicate = new ArrayList<>();


        for (int i = 0; i < letters.length-1; i++){
            for (int j = i+1; j < letters.length; j++){
                if(letters[i] == letters[j]){
                    duplicate.add(letters[i]);
                }
            }
        }

        for (int i = 0; i < letters.length; i++){
            if(duplicate.contains(letters[i])){
                letters[i] = ')';
            } else {
                letters[i] = '(';
            }
        }

        return String.valueOf(letters);
    }
}
