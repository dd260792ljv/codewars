package CodeWars;

import java.util.Arrays;

public class MakeArrayConsecutive {
    public static void main(String[] args) {

        int [] statues = {2,1,7,6};

        makeArrayConsecutive(statues);
    }


    public static int makeArrayConsecutive(int[] statues) {

        Arrays.sort(statues);

        int sLastNumber = statues[statues.length-1];
        int sFirstNumber = statues[0];
        int sTotalCount = sLastNumber - sFirstNumber + 1;
        int sAbsentCount = sTotalCount - statues.length;

        int[] array = new int[sAbsentCount];

        for(int i = 0,  number = sFirstNumber, k = 0;  number <= sLastNumber;){
            if(statues[i]==number){
                i++;
                number++;
            }else {array[k]=number++;
                k++;}
        }

        for (int i = 0; i < array.length; i++) {
            System.out.print(array[i] + "  ");
        }

        System.out.println(sAbsentCount);

        return sAbsentCount;

    }
}
