package CodeWars;

import java.util.ArrayList;
import java.util.List;

public class DigPow {
    public static void main(String[] args) {
        System.out.println(digPow(89,1));
    }

    public static long digPow(int n, int p) {


        List<Integer> arr = new ArrayList<Integer>();

        int m = n;

        do {
            arr.add(m % 10);
            m /= 10;
        } while (m != 0);

        int a = 0;

        for (int i = arr.size()-1, j = p; i >= 0; i--, j++){
            a +=  Math.pow(arr.get(i),j);
            }

            return (a%n == 0) ? a/n : -1;

        }
}
