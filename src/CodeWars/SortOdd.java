package CodeWars;

import java.lang.reflect.Array;
import java.util.Arrays;

public class SortOdd {

    public static void main(String[] args) {
        System.out.println(sortArray(new int[]{5, 3, 2, 8, 1, 4}));
    }

    public static String sortArray(int[] array) {
        for (int i = 0; i < array.length - 1; i++) {
            for (int j = i + 1; j < array.length; j++) {
                if ((array[i] % 2) != 0 && array[j] % 2 != 0) {
                    if (array[i] > array[j]) {
                        int temp = array[i];
                        array[i] = array[j];
                        array[j] = temp;
                    }
                }
            }
        }
        return Arrays.toString(array);
    }
}