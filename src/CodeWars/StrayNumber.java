package CodeWars;

import java.util.Arrays;

public class StrayNumber {
    public static void main(String[] args) {
        System.out.println(stray(new int[]{17, 17, 3, 17, 17, 17, 17}));
    }

    static int stray(int[] numbers) {

        int answer = 0;
        for (int i = 0, j = i + 1; i < numbers.length; i++) {

            if (numbers[i] != numbers[j]) {
                if (numbers[i] == numbers[j+1]) {
                    answer = numbers[j+1];
                } else {
                    answer = numbers[i];
                }
            }
        }

        return answer;
    }

    static int strayTwo(int[] numbers) {
        Arrays.sort(numbers);
        return numbers[0] == numbers[1] ? numbers[numbers.length-1] : numbers[0];
    }
}
