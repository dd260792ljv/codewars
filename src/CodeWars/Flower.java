package CodeWars;

public class Flower {
    public static void main(String[] args) {
        System.out.println(howMuchILoveYou(75));
    }

    public static String howMuchILoveYou(int nb_petals) {

        String answer = "";
            switch (nb_petals%6) {
                case 1:
                    answer = "I love you";
                    break;
                case 2:
                    answer = "a little";
                    break;
                case 3:
                    answer = "a lot";
                    break;
                case 4:
                    answer = "passionately";
                    break;
                case 5:
                    answer = "madly";
                    break;
                case 0:
                    answer = "not at all";
                    break;
                default:
                    answer = "null";
                    break;
            }
            return answer;
    }
}
