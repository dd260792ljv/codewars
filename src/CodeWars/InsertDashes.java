package CodeWars;
public class InsertDashes {
    public static void main(String[] args) {
        System.out.println(insertDash(1003567));
    }

    public static String insertDash(int num) {

        String n = String.valueOf(num);
        String answer = "";

        for (int i = 0; i < n.length() - 1; i++) {
            answer += String.valueOf(n.charAt(i));
            if ((n.charAt(i) - '0') % 2 != 0 && (n.charAt(i + 1) - '0') % 2 != 0) {
                answer += "-";
            }
        }
        return answer + String.valueOf(n.charAt(n.length()-1));
    }
}
