package CodeWars;

public class Kata {

    public static void main(String[] args) {
        System.out.println(compare("", "zz1"));
    }

    public static Boolean compare(String s1, String s2)
    {

        if(s1==null ){
            s1="";
        }

        if(s2==null ){
            s2="";
        }


        s1 = s1.toUpperCase();
        s2 = s2.toUpperCase();
        char [] c1 = s1.toCharArray();
        char [] c2 = s2.toCharArray();

        for (int i = 0; i<c1.length; i++){
            if ((int) c1[i] < 65 || (int) c1[i] > 90 ) {
                c1 = new char[0];
                }
        }

        for (int i = 0; i<c2.length; i++){
            if ((int) c2[i] < 65 || (int) c2[i] > 90 ) {
                c2 = new char[0];
            }
        }

        int i1 = 0;
        int i2 = 0;

        for (int i=0; i<c1.length; i++){
            i1 += (int) c1[i];
        }

        for (int i=0; i<c2.length; i++){
            i2 += (int) c2[i];
        }

        return i1==i2;

    }
}
