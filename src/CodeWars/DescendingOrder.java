package CodeWars;

public class DescendingOrder {
    public static void main(String[] args) {
        System.out.println(sortDesc(21445));
    }

    public static int sortDesc(final int num) {



        String n = String.valueOf(num);
        int [] result = new int[n.length()]; //Требуемый массив
        for (int i = 0; i < n.length(); i++) result[i] = n.charAt(i)-'0';

        for (int i = result.length-1; i>0; i--){
            for (int j = 0; j<i; j++){
                if(result[j]<result[j+1]){

                    int temp = result[j];
                    result[j] = result[j+1];
                    result[j+1] = temp;

                }
            }
        }

        int output = 0;
        for(int i = 0; i< result.length; i++){
            output = output*10 + result[i];
        }

        return output;

        }
}
