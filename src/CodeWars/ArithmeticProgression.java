package CodeWars;

import java.util.ArrayList;

public class ArithmeticProgression {
    public static void main(String[] args) {
        System.out.println(arithmeticSequenceElements(1,2,5));
    }



    public static String arithmeticSequenceElements(int first, int step, long total) {
        ArrayList<Integer> a = new ArrayList();
        for (int i = first; a.size()< total; i=i+step){
            a.add(i);
        }

        return a.toString().replace('[',' ').replace(']',' ').trim();
    }

    public static String arithmeticSequence(int first, int step, long total) {
        String text = first + "";
        for (int i = 0; i < total - 1; i++) {
            first += step;
            text += ", " + first;
        }
        return text;
    }
}
