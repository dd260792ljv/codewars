package CodeWars;

public class PerfectSquare {
    public static void main(String[] args) {
        System.out.println(findNextSquare(121));
    }

    public static long findNextSquare(long sq) {
        double a = Math.sqrt(sq) + 1;
        return a%1 ==0 ? (long) (a*a) : -1;
    }
}
