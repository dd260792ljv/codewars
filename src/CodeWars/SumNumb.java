package CodeWars;

public class SumNumb {

    public static void main(String[] args) {
        System.out.println(getSum(-1,5));
        System.out.println(getSumTwo(-1,5));
    }

    public static int getSum(int a, int b){

        int sum =0;
        if(a<b) {
            for (int i = a; i <= b ; i++) {
                sum += i;
            }
        }
        else if (a>b){
            for (int i = b; i <= a; i++) {
                sum += i;
            }
        }
        else if (a==b){
            sum = a;
        }

        return sum;

        }

    public static int getSumTwo(int a, int b){

        int sum = 0;
        for(int i = Math.min(a,b); i<=Math.max(a,b); i++){
            sum +=i;
        }

        return a==b ? a : sum;

        }
}
