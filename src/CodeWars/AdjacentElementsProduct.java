package CodeWars;


import java.io.IOException;

public class AdjacentElementsProduct {
    public static void main(String[] args) throws IOException {

        int[] numbers = new int[]{2, 1, -2, -5, 7, 6};

        int product;
        int secondMax;

        product = adjacentElementsProduct(numbers);
        secondMax = secondMax(numbers);
        System.out.println(product);
        System.out.println(secondMax);
    }

    public static int adjacentElementsProduct(int[] inputArray) {
        int product = inputArray[0] * inputArray[1];
        for (int i = 1; i < inputArray.length - 1; i++) {
            int temp = inputArray[i] * inputArray[i + 1];
            if (product < temp)
                product = temp;
        }
        return product;
    }

    public static int secondMax(int[] inputArray) {
        int max;
        int secondMax;

        if (inputArray[0] > inputArray[1]) {
            max = inputArray[0];
            secondMax = inputArray[1];
        } else {
            max = inputArray[1];
            secondMax = inputArray[0];
        }


        for (int i = 2; i < inputArray.length; i++) {
            if ((max >= inputArray[i]) && secondMax < inputArray[i])
                secondMax = inputArray[i];

            if (max < inputArray[i]) {
                secondMax = max;
                max = inputArray[i];
            }
        }

        return secondMax;
    }
}
