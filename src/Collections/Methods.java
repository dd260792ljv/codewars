package Collections;

import java.util.Collection;
import java.util.Map;

public class Methods {

    public static long add(Collection collection) {
        long start = System.currentTimeMillis();
        for (int i = 0; i < 100000; i++) {
            collection.add(i);
        }
        long end = System.currentTimeMillis() - start;
        return end;
    }

    public static long add(Map map) {
        long start = System.currentTimeMillis();
        for (int i = 0; i < 100000; i++) {
            map.put(i,i);
        }
        long end = System.currentTimeMillis() - start;
        return end;
    }


    public static long get(Collection collection, int index){
        long start = System.currentTimeMillis();
        collection.contains(index);
        long end = System.currentTimeMillis() - start;
        return end;
    }

    public static long get(Map map, int index){
        long start = System.currentTimeMillis();
        map.get(index);
        long end = System.currentTimeMillis() - start;
        return end;
    }


    public static long remove(Collection collection, int index){
        long start = System.currentTimeMillis();
        collection.remove(index);
        long end = System.currentTimeMillis() - start;
        return end;
    }

    public static long remove(Map map, int index){
        long start = System.currentTimeMillis();
        map.remove(index);
        long end = System.currentTimeMillis() - start;
        return end;
    }
}
