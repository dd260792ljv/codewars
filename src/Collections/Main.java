package Collections;


import java.util.*;

import static Collections.Methods.add;
import static Collections.Methods.get;
import static Collections.Methods.remove;


public class Main {

    public static void main(String[] args) {

        ArrayList myArrayList = new ArrayList();
        LinkedList myLinkedList = new LinkedList();
        HashSet myHashSet = new HashSet();
        TreeSet myTreeSet = new TreeSet();
        HashMap myHashMap = new HashMap();
        TreeMap myTreeMap = new TreeMap();

        System.out.println("ArrayList add 100 000 elements: " + add(myArrayList) + " ms");
        System.out.println("ArrayList get element: " + get(myArrayList, 82818) + " ms");
        System.out.println("ArrayList remove element: " + remove(myArrayList, 43004) + " ms");

        System.out.println("------------------------------------------");

        System.out.println("LinkedList add 100 000 elements: " + add(myLinkedList) + " ms");
        System.out.println("LinkedList get element: " + get(myLinkedList, 82818) + " ms");
        System.out.println("LinkedList remove element: " + remove(myLinkedList, 43004) + " ms");

        System.out.println("------------------------------------------");

        System.out.println("HashSet add 100 000 elements: " + add(myHashSet) + " ms");
        System.out.println("HashSet get element: " + get(myHashSet, 82818) + " ms");
        System.out.println("HashSet remove element: " + remove(myHashSet, 43004) + " ms");

        System.out.println("------------------------------------------");

        System.out.println("TreeSet add 100 000 elements: " + add(myTreeSet) + " ms");
        System.out.println("TreeSet get element: " + get(myTreeSet, 82818) + " ms");
        System.out.println("TreeSet remove element: " + remove(myTreeSet, 43004) + " ms");

        System.out.println("------------------------------------------");

        System.out.println("HashMap add 100 000 elements: " + add(myHashMap) + " ms");
        System.out.println("HashMap get element: " + get(myHashMap, 82818) + " ms");
        System.out.println("HashMap remove element: " + remove(myHashMap, 43004) + " ms");

        System.out.println("------------------------------------------");

        System.out.println("TreeMap add 100 000 elements: " + add(myTreeMap) + " ms");
        System.out.println("TreeMap get element: " + get(myTreeMap, 82818) + " ms");
        System.out.println("TreeMap remove element: " + remove(myTreeMap, 43004) + " ms");

    }
}
