package Calculators.pavloCalc;

import java.util.ArrayList;
import java.util.List;


class PrettyString {

    Double cut(Double input){
        return (double) Math.round(input*1000)/1000;
    }

    String doubleToInteger(Double input){
        char [] cArray = Double.toString(input).toCharArray();
        List<String> list = new ArrayList<>(cArray.length);
        for(char c : cArray) {
            list.add(String.valueOf(c));
        }

        if (list.get(list.size() - 1).equals("0") & list.indexOf(".")==list.size()-2){
            list.subList(list.indexOf("."),list.size()).clear();
        }

        return list.toString().replaceAll("[\\[,\\], \\s]","");
    }
}
