package Calculators.pavloCalc;

import java.util.Scanner;
import java.util.Stack;

class RPNCalc {

    private static boolean nextIsOperator(String next) {
        return (next.equals("+") || next.equals("-") || next.equals("*") || next.equals("/"));
    }

    static Double rpnCalc(String input) {

        input = input.trim();
        String next;
        Stack<Double> stack = new Stack<>();
        Scanner scan = new Scanner(input);

        while (scan.hasNext()) {
            next = scan.next();

            if (nextIsOperator(next)) {
                if (stack.size() > 1) {
                    switch (next) {
                        case "+":
                            stack.push( stack.pop() + stack.pop());
                            break;
                        case "-":
                            stack.push(- stack.pop() + stack.pop());
                            break;
                        case "*":
                            stack.push(stack.pop() * stack.pop());
                            break;
                        case "/":
                            double first = stack.pop();
                            double second = stack.pop();

                            if (first == 0) {
                                throw new IllegalArgumentException("Division by zero");
                            } else {
                                stack.push(second / first);
                            }
                            break;
                    }
                } else {
                    System.out.println("Operator appears before there were enough operands values");
                }
            } else {
                try {
                    stack.push(Double.parseDouble(next));
                } catch (NumberFormatException c) {
                    System.out.println("Input string is not a valid");
                }
            }
        }

        if (stack.size() > 1) {
            System.out.println("There is too many operands and not enough operators");
        }

        return stack.pop();
    }
}
