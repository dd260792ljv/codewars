package Calculators.pavloCalc;

import java.util.*;

public class InfixToPostfixConverter {

   static ArrayList <String> operatorsArray = new ArrayList<String>(){
        {
            add(0,"+");
            add(1, "-");
            add(2,"*");
            add(3,"/");
        }
    };

    private static boolean isHigerPriority(String op, String sub)
    {
        return (operatorsArray.contains(sub) && operatorsArray.indexOf(sub) >= operatorsArray.indexOf(op));
    }

    public static String postfix(String infix)
    {
        StringBuilder output = new StringBuilder();
        Deque<String> stack  = new LinkedList<>();

        for (String token : infix.split("\\s+")) {
            if (operatorsArray.contains(token)) {
                while ( ! stack.isEmpty() && isHigerPriority(token, stack.peek()))
                    output.append(stack.pop()).append(' ');
                stack.push(token);

            } else if (token.equals("(")) {
                stack.push(token);

            } else if (token.equals(")")) {
                while ( ! stack.peek().equals("("))
                    output.append(stack.pop()).append(' ');
                stack.pop();

            } else {
                output.append(token).append(' ');
            }
        }

        while ( ! stack.isEmpty())
            output.append(stack.pop()).append(' ');

        return output.toString();
    }
}
