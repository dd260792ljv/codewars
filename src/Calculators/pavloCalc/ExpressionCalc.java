package Calculators.pavloCalc;

class ExpressionCalc {

    static String calculate(String expression){
        PrettyString ps = new PrettyString();

        String rpnString = InfixToPostfixConverter.postfix(expression);

        Double toCut = RPNCalc.rpnCalc(rpnString);

        return ps.doubleToInteger(ps.cut(toCut));
    }

}
