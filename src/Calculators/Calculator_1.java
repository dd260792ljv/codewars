package Calculators;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Calculator_1 {

    public static void main(String[] args) throws IOException {

        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        String a;
        String b;
        String operator;

        do {
            System.out.print("Enter a first number: ");
            a = reader.readLine();
        } while (!a.matches("[-+]?[\\d]+[.]?[\\d]*"));

        do {
            System.out.print("Enter a operation: ");
            operator = reader.readLine();
        } while (!operator.matches("[+*/-]"));

        do {
            System.out.print("Enter a second number: ");
            b = reader.readLine();
        } while (!b.matches("[\\d]+[.]?[\\d]*"));

        switch (operator) {
            case "+":
                System.out.println("= " + add(a, b));
                break;
            case "-":
                System.out.println("= " + sub(a, b));
                break;
            case "*":
                System.out.println("= " + mul(a, b));
                break;
            case "/":
                if (b.equals("0")) {
                    System.out.println("Division by zero.");
                } else {
                    System.out.println("= " + div(a, b));
                }
                break;
        }
    }

    public static double add(String a, String b) {
        return Double.parseDouble(a) + Double.parseDouble(b);
    }

    public static double sub(String a, String b) {
        return Double.parseDouble(a) - Double.parseDouble(b);
    }

    public static double mul(String a, String b) {
        return Double.parseDouble(a) * Double.parseDouble(b);
    }

    public static double div(String a, String b) {
        return Double.parseDouble(a) / Double.parseDouble(b);
    }
}
