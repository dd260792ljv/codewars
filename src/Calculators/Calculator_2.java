package Calculators;

public class Calculator_2 {


    public String calculate (String a, String b, String operator){
        switch (operator) {
            case "+":
                return "= " + add(a, b);
            case "-":
                return "= " + sub(a, b);
            case "*":
                return "= " + mul(a, b);
            case "/":
                if (b.equals("0")) {
                    return "Division by zero.";
                } else {
                    return "= " + div(a, b);
                }
                default:
                    return "null";
        }
    }


    public static double add(String a, String b) {
        return Double.parseDouble(a) + Double.parseDouble(b);
    }

    public static double sub(String a, String b) {
        return Double.parseDouble(a) - Double.parseDouble(b);
    }

    public static double mul(String a, String b) {
        return Double.parseDouble(a) * Double.parseDouble(b);
    }

    public static double div(String a, String b) {
        return Double.parseDouble(a) / Double.parseDouble(b);
    }

}
