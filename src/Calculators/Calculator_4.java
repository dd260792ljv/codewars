package Calculators;

import java.util.Scanner;

public class Calculator_4 {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        double res = 0;

        System.out.println("Введите выражение для расчета");
        String line = "";
        line = sc.nextLine();
        String[] numb = line.split("[+-/*]");
        double[] numbers = new double[numb.length];
        for (int i = 0; i < numbers.length; i++) {
            numbers[i] = Double.valueOf(numb[i]);
        }
        String[] operator = line.split("\\d+");
        res = numbers[0];
        for (int i = 1; i < operator.length; i++) {
            if (operator[i].compareTo("+") == 0) {
                res = res + numbers[i];
            }
            if (operator[i].compareTo("-") == 0) {
                res = res - numbers[i];
            }
            if (operator[i].compareTo("/") == 0) {
                res = res / numbers[i];
            }
            if (operator[i].compareTo("*") == 0) {
                res = res * numbers[i];
            }
        }
        System.out.println(res);
        sc.close();
    }
}
